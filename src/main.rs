extern crate libusb;
extern crate libc;
extern crate signal_hook;
use libusb::io::sync::*;
use libusb::DeviceHandleSyncApi; 

use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::slice;
use std::thread;
use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;
use std::os::unix::net::{UnixStream, UnixListener};
use std::io::{Read, Write};
use std::time::Duration;

fn process_control_sock(
    mut stream: UnixStream,
    sender: &Sender<String>,
    receiver: &Receiver<String>,
) {
    println!("New unix connection");
    stream.set_nonblocking(false);
    let mut nomore = -1;
    stream.set_read_timeout(Some(Duration::new(1, 0))).unwrap();
    loop {
        let mut control_buffer = Vec::new();
        match stream.read_to_end(&mut control_buffer) {
            Ok(size) => {
                if size > 0 {
                    let s = String::from_utf8_lossy(&control_buffer);
                    println!("{}", s);
                    sender.send(s.to_string());
                    nomore = 0;
                } else {
                    nomore += 1;
                }
            }
            Err(err) => {
                println!("{}", err);
            }
        }

        let s = receiver.try_recv().unwrap_or("".to_string());
        if s.len() > 0 {
            stream.write(s.as_bytes());
            println!("{}", s);
        } else if nomore == 100 {
            break;
        }
    }
    println!("Connection dropped");
}

// Blocking socket listener
fn socket_listener(busaddress: String, sender: Sender<String>, receiver: Receiver<String>, signal: Receiver<i32>) {

    let sockname =  format!("/tmp/stmcontrol-{}.sock", busaddress);
    let listener = UnixListener::bind(&sockname).unwrap();
    listener.set_nonblocking(true);
    for stream in listener.incoming() {
        match stream {
            Ok(stream) => process_control_sock(stream, &sender, &receiver),
            Err(_) => {
                // No connection
                thread::sleep(Duration::from_millis(100));
            }
        }

        let s = signal.try_recv().unwrap_or(0);
        if s == 1 {
            break;
        }
    }

    std::fs::remove_file(sockname);
}

struct UsbThread {
    handle: std::thread::JoinHandle<()>,
    tx: mpsc::Sender<i32>,
    ctx: std::sync::Arc<libusb::io::sync::Context>
}

impl UsbThread {
    fn new(ctx: std::sync::Arc<libusb::io::sync::Context>, handle: std::thread::JoinHandle<()>, tx: mpsc::Sender<i32>) -> Self {
        UsbThread {
            tx : tx,
            handle  : handle,
            ctx: ctx
        }
    }

}

fn open_device(threads: &mut Vec<UsbThread>) -> i32{
    let context = Arc::new({
        libusb::io::sync::Context::new().unwrap()
    });

    let mut stm_count = 0;
    let devices = match context.clone().devices() {
        Ok(d) => d,
        Err(_) => return 0
    };

    for device in devices.iter() {
        let device_desc = match device.device_descriptor() {
            Ok(d) => d,
            Err(_) => continue
        };
        if device_desc.vendor_id() == 0x0483 && device_desc.product_id() == 0x5740 {
            println!("Found stm32 woho lets play :D");
            let (usb_thread_tx, usb_thread_rx) = mpsc::channel();
            match device.open() {
                Ok(mut cdc) => {
                    let busaddress = format!("{}-{}", device.bus_number(), device.address());
                    let mut thread = thread::spawn(move || {
                        let buf = [];
                        cdc.detach_kernel_driver(1);
                        cdc.claim_interface(1);
                        /* Send fake baudrate + DTR on control channel
                         * to tell STM to "open" comport */
                        cdc.write_control(0x21, 0x22, 0x3, 0, &buf, Duration::from_secs(1));
                        let mut vec = Vec::<u8>::with_capacity(64);
                        let (sock_tx, sock_rx) = mpsc::channel();
                        // bulkin_tx sender is USB IN
                        // bulkin_rx receiver is sock.
                        let (bulkin_tx, bulkin_rx) = mpsc::channel();
                        // bulkout_tx sender is sock
                        // bulkin_rx receiver is USB OUT
                        let (bulkout_tx, bulkout_rx) = mpsc::channel();
                        let handle = thread::spawn(|| socket_listener(busaddress, bulkout_tx, bulkin_rx, sock_rx));
                        loop {
                            let inbuffer =
                            unsafe { slice::from_raw_parts_mut((&mut vec[..]).as_mut_ptr(), 64) };
                            cdc.read_bulk(0x81, inbuffer, Duration::from_millis(1))
                               .map(|size|{
                                    let data = String::from_utf8_lossy(&inbuffer[0..size]);
                                    bulkin_tx.send(data.to_string());
                            });
                            let s = bulkout_rx.try_recv().unwrap_or("".to_string());
                            if s.len() > 0 {
                                println!("{}", &s);
                                // yes, lets forward it to STM
                                cdc.write_bulk(0x1, s.as_bytes(), Duration::from_millis(1));
                            }

                            let s = usb_thread_rx.try_recv().unwrap_or(0);
                            if s == 1 {
                                sock_tx.send(1);
                                handle.join();
                                break;
                            }
                        }

                    });
                    threads.push(UsbThread::new(context.clone(), thread, usb_thread_tx));
                 },
                 Err(e) => eprintln!("{}", e),
             };
             stm_count+=1;
        }
    }

    stm_count
}

fn main() {
    let term = Arc::new(AtomicBool::new(false));
    signal_hook::flag::register(signal_hook::SIGQUIT, Arc::clone(&term)).unwrap();
    signal_hook::flag::register(signal_hook::SIGTERM, Arc::clone(&term)).unwrap();
    signal_hook::flag::register(signal_hook::SIGINT, Arc::clone(&term)).unwrap();

    // post when a UNIX signal is recieved such as SIGTERM...
    let (sig_tx, sig_rx) = mpsc::channel();
    let mut threads: Vec<UsbThread> = vec!();


    // packet posted using socat or similar those messages are
    // forwarded to STM and packet received from USB are posted
    // to socket
    open_device(&mut threads);
    /*
    for device in devices.unwrap().iter().as_owned() {
        let device_desc = device.device_descriptor().unwrap();
        println!(
            "{:04X}:{:04X}",
            device_desc.vendor_id(),
            device_desc.product_id()
        );
        if device_desc.vendor_id() == 0x0483 && device_desc.product_id() == 0x5740 {
       }
                    let thread_usb_enumerate = thread::spawn(move || {
                        let mut vec = Vec::<u8>::with_capacity(64);
                        let mut inbuffer =
                            unsafe { slice::from_raw_parts_mut((&mut vec[..]).as_mut_ptr(), 64) };
                        let (sock_tx, sock_rx) = mpsc::channel();
                        // bulkin_tx sender is USB IN
                        // bulkin_rx receiver is sock.
                        let (bulkin_tx, bulkin_rx) = mpsc::channel();
                        // bulkout_tx sender is sock
                        // bulkin_rx receiver is USB OUT
                        let (bulkout_tx, bulkout_rx) = mpsc::channel();
                        thread::spawn(|| socket_listener(bulkout_tx, bulkin_rx, sock_rx));
                        loop {
                            /* read from bulk IN channel
                             * unwrap error and return 0 in case
                             * read failed */
                            let size = handle
                                .read_bulk(0x81, inbuffer, Duration::from_millis(1))
                                .unwrap_or(0);
                            if size > 0 {
                                let s = String::from_utf8_lossy(&inbuffer[0..size]);
                                bulkin_tx.send(s.to_string());
                            }
                            // Did we get any characters from unix socket?
                            let s = bulkout_rx.try_recv().unwrap_or("".to_string());
                            if s.len() > 0 {
                                println!("{}", &s);
                                // yes, lets forward it to STM
                                handle.write_bulk(0x1, s.as_bytes(), Duration::from_millis(1));
                            }

                            let s = usb_thread_rx.try_recv().unwrap_or(0);
                            if s == 1 {
                                break;
                            }


                        }
                    });
                }
                Err(v) => {
                    println!("Open failed {}", v);
                }
            }
        }
    */
    println!("enumerate done");

    loop {
        if term.load(Ordering::Relaxed) {
            println!("took it");
            sig_tx.send(2);
        }

        let bFound = sig_rx.try_recv().unwrap_or(0);

        if bFound == 2 {
            // main looooop, just wait for SIGINT, SIGTERM
            for mut thread in threads {
                thread.tx.send(1);
                thread.handle.join();
            }
 //           usb_signal_tx.send(1);
//            thread_usb_enumerate.join().unwrap();
//            unix_signal_tx.send(1);
//            thread_unix.join().unwrap();
            break;
        } else if bFound == 1 {
            println!("No STM in USB CDC mode found!");
//            unix_signal_tx.send(1);
  //          match thread_unix.join() {
    //            Ok(_) => {}
     //           Err(e) => println!("unix dead {:?}", e),
            //};
       //     break;
        }
        thread::sleep(Duration::from_millis(100));
    }

    println!("main thread died");
}
